// Create a simple server and the following routes with their corresponding HTTP methods and responses:
let http = require("http");

let app = http.createServer((req, res) => {
  // - If the url is http://localhost:4000/, send a response Welcome to Booking System
  if (req.url == "/" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write("Welcome to Booking System");
    res.end();
  }
  // - If the url is http://localhost:4000/profile, send a response Welcome to your profile!
  if (req.url == "/profile" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write("Welcome to your profile!");
    res.end();
  }
  // - If the url is http://localhost:4000/courses, send a response Here’s our courses available
  if (req.url == "/courses" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write("Here’s our courses available");
    res.end();
  }
  // - If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
  if (req.url == "/addcourse" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write("Add a course to our resources");
    res.end();
  }
  // - If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
  if (req.url == "/updatecourse" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write("Update a course to our resources");
    res.end();
  }
  // - If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
  if (req.url == "/archivecourses" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write("Archive courses to our resources");
    res.end();
  }
});

//Do not modify
//Make sure to save the server in variable called app
if (require.main === module) {
  app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
