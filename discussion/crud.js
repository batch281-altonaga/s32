let http = require("http");

// Mock Database
let directory = [
  {
    name: "Brandon",
    email: "brandon@mail.com",
  },
  {
    name: "Jobert",
    email: "jobert@mail.com",
  },
];

let port = 4000;

let app = http.createServer((req, res) => {
  // Route for returning all items upon receiving a GET request
  if (req.url == "/users" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "application/json" });
    res.write(JSON.stringify(directory));
    res.end();

    // Route for creating a new data upon receiving a POST request
  }
  if (req.url == "/users" && req.method == "POST") {
    // body serves as the container for the data
    let body = "";

    // Stream is a sequence of data

    req.on("data", (chunk) => {
      // chunk is a part of the data
      body += chunk;
    });

    // End is called when all data has been received

    req.on("end", () => {
      // body is now the container for the whole data
      body = JSON.parse(body);
    });

    // Creates a new object representing the new mock database
    let newUser = {
      name: body.name,
      email: body.email,
    };

    // Add the new user to the mock database
    directory.push(newUser);
    console.group(directory);

    res.writeHead(200, { "Content-Type": "application/json" });
    res.write(JSON.stringify(directory));
    res.end();
  }
});

app.listen(port, () => console.log("Server is running on port " + port));

// When i send request, it is returning undefined
// I think the problem is in the body variable
// I tried to console.log the body variable and it is returning undefined
